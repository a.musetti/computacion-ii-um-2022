#!/usr/bin/env python3
# Requerimos Python 3

import getopt, sys, os
from typing import NoReturn
from threading import Thread, Event

def ayuda() -> NoReturn:
    print('''uso: %s -f ARCHIVO
    ''' % sys.argv[0], file=sys.stderr)
    sys.exit(1)

# Main.

ruta_archivo = None

(opt,arg) = getopt.getopt(sys.argv[1:], 'f:')
for (op,ar) in opt:
    if op == '-f':
        ruta_archivo = ar

if ruta_archivo is None:
    ayuda()


linea = None
salir = False

def leer_entrada(evLinea):
	global linea, salir
	while not salir:
		linea = sys.stdin.readline()
		if len(linea) == 0 or linea == "bye\n":
			salir = True
		evLinea.set()

def escribir_salida(evLinea,archivo):
	global linea, salir
	while not salir:
		evLinea.wait()
		if not salir:
			archivo.write(linea.upper())
			archivo.flush()
		evLinea.clear()

with open(ruta_archivo,'w') as archivo:
	eventoLinea = Event()
	h1 = Thread(target=leer_entrada,args=(eventoLinea,),daemon=True)
	h2 = Thread(target=escribir_salida,args=(eventoLinea,archivo,),daemon=True)
	h1.start()
	h2.start()
	h1.join()
	h2.join()
