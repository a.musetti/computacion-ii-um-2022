#!/usr/bin/env python3
# Requerimos Python 3

import argparse, sys, os, math
from typing import NoReturn

from funciones import *
from celery import group

MATRIZ='matriz' # Calcular toda la matriz en un mega grupo de Celery.
FILAS='filas' # Calgular por filas usando Groups de Celery.
TRAMOS='tramos' # Calcular por filas usando Chunks de Celery.
modos=[MATRIZ,FILAS,TRAMOS]

mapa_funciones={
    'raiz' : raiz,
    'pot'  : pot,
    'log'  : log,
    'doble': doble,
}
nombres_funciones = mapa_funciones.keys()


# Analizar argumentos.
argp = argparse.ArgumentParser(
    prog=sys.argv[0],
    description='Calcular funciones sobre celdas de una matriz usando Celery.',
)
argp.add_argument('-m','--modo',dest='modo',default=MATRIZ,choices=modos,required=False,help='Modo de calcular la matriz.')
argp.add_argument('-c','--calculo',dest='calculo',choices=nombres_funciones,required=True,help='Cálculo a realizar por cada celda.')
argp.add_argument('-f','--fichero',dest='fichero',required=True,help='Archivo que contiene la matriz.')
args=argp.parse_args()
ruta_archivo = args.fichero
funcion = args.calculo
modo = args.modo

def matriz(archivo):
    # OJO: SIN CONTROL DE ERRORES.
    # Asumimos entrada bien formada:
    #  - todas las filas del mismo largo.
    #  - cada fila con todos los separadores.
    #  - números decimales en cada celda.
    return [[float(x.strip()) for x in linea.strip().split(',')]
            for linea in archivo]

def vermat(m):
    for f in m:
        for x in f:
            print("%15.3f" % x,end='')
        print()


# Crea una lista plana desde una matriz rectangular.
def aplanar(matriz):
    return [celda for fila in matriz for celda in fila]

# Crea una matriz rectangular desde una lista plana.
def rectangular(lista,largo_fila):
    return [lista[i:i+largo_fila] for i in range(0,len(lista),largo_fila)]


# Calcula elemento uno a uno en un grupo gigante del tamaño de la matriz (MxN).
# Aplana toda la matriz, crea un grupo con todos los elementos,
# ejecuta el grupo en Celery y luego rearma la matriz de resultados
# con la lista de resultados del grupo.
def calcular_matriz(f,m):
    largo_fila=len(m[0])
    result=group(f.s(x) for x in aplanar(m))
    return rectangular(result.delay().get(),largo_fila)

# Calcula por filas usando "groups" de Celery.
# Mejor que un mega-grupo con todos los elementos de la matriz,
# pero se manda un grupo por cada fila.
def calcular_por_filas(f,m):
    # Creamos un grupo por cada fila.
    r=[group(f.s(x) for x in fila) for fila in m]
    # Mandamos cada grupo a calcular.
    r=[fila.delay() for fila in r]
    # Obtenemos los resultados de cada fila.
    r=[fila.get() for fila in r]
    return r

# Calcula por filas usando "chunks" de Celery.
# Mejor que por grupos porque manda una sola tarea,
# que en Celery se ejecuta por tramos (filas).
def calcular_por_tramos(f,m):
    largo_fila=len(m[0])
    # El zip(...) es importante... me volvió loco...
    return f.chunks(zip(aplanar(m)),largo_fila)().get()

mapa_modos={
    MATRIZ: calcular_matriz,
    FILAS: calcular_por_filas,
    TRAMOS: calcular_por_tramos,
}

# Encabezado.
print('; '.join([f'{x[0]}: {x[1]}' for x in zip(['archivo','funcion','modo'],[ruta_archivo,funcion,modo])]))
with open(ruta_archivo,'r') as archivo:
    m=matriz(archivo)
    f=mapa_funciones[funcion]
    r=mapa_modos[modo](f,m)
    vermat(r)
