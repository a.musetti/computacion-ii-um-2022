from celery import Celery

app = Celery('funciones_config', broker='redis://localhost', backend='redis://localhost', include=['funciones'])
