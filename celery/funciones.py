import math

from funciones_config import app

@app.task
def raiz(x):
    return math.sqrt(x)

@app.task
def pot(x):
    return math.pow(x,x)

@app.task
def log(x):
    return math.log(x,10)

@app.task
def doble(x):
    return 2*x
