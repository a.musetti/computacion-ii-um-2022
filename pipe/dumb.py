#!/usr/bin/env python3
# Requerimos Python 3

# Implementacion directa de invertir lineas de un archivo.
# Para tener idea del costo de fork + pipes en archivos grandes,
# comparando tiempos de pipe.py contra dumb.py

import getopt, sys, os
from typing import NoReturn

def ayuda() -> NoReturn:
    print('''uso: %s -f ARCHIVO
	Invierte las lineas de un archivo.
    ''' % sys.argv[0])
    sys.exit(1)

def invertir_lineas(archivo):
    for linea in archivo:
        linea = linea[:-1] # Remover fin de linea.
        inver = linea[::-1] # Invertir.
        print(inver)

# Main.

ruta_archivo=None

(opt,arg) = getopt.getopt(sys.argv[1:], 'f:')
for (op,ar) in opt:
    if op == '-f':
        ruta_archivo = ar

if ruta_archivo is None:
    ayuda()

with open(ruta_archivo, 'r') as archivo:
    invertir_lineas(archivo)
