#!/bin/sh

cd `dirname $0`

FILE=${1:-GPL.txt}
LINES=`wc -l $FILE | cut -d\  -f1`
BYTES=`wc -c $FILE | cut -d\  -f1`
echo >&2 === ARCHIVO: $FILE LINEAS: $LINES BYTES: $BYTES ===
for py in ./dumb.py ./pipe.py; do
    echo >&2
    echo >&2 ==== $py ====
    time $py -f $FILE
done

