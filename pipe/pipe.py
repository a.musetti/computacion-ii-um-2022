#!/usr/bin/env python3
# Requerimos Python 3

import getopt, sys, os
from typing import NoReturn

def ayuda() -> NoReturn:
    print('''uso: %s -f ARCHIVO
	Invierte las lineas de un archivo.
    ''' % sys.argv[0])
    sys.exit(1)

def invertir_lineas(archivo):
    rr=[] # Guardar pipes de lectura para el padre.
    # No leemos en el padre inmediatamente luego de fork()
    # porque si no quedaria un programa secuencial.
    # Dejar a los hijos que terminen y leemos todo junto.
    for linea in archivo:
        linea = linea[:-1] # Remover fin de linea.
        # Abrir tuberias.
        (rh,wh) = os.pipe()
        (rp,wp) = os.pipe()
        if os.fork() == 0:
            # En el hijo.
            # Esperar una linea y devolverla invertida.
            os.close(rp)
            os.close(wh)
            rh = os.fdopen(rh)
            wp = os.fdopen(wp, 'w')
            linea = rh.read()
            inver = linea[::-1] # Invertir linea.
            wp.write(inver)
            wp.close()
            sys.exit(0) # NoReturn
        else:
            # En el padre.
            os.close(rh)
            os.close(wp)
            rp = os.fdopen(rp)
            wh = os.fdopen(wh, 'w')
            # Guardar fd lector para el padre.
            # Leemos cuando todos los hijos hayan terminado.
            # Si leyeramos aca, el programa quedaria secuencial.
            rr.append(rp)
            # Mandar linea a este hijo.
            wh.write(linea)
            wh.close()
    # Esperar hijos y mostrar las lineas invertidas.
    os.wait()
    for r in rr:
        print(r.read())


# Main.

ruta_archivo=None

(opt,arg) = getopt.getopt(sys.argv[1:], 'f:')
for (op,ar) in opt:
    if op == '-f':
        ruta_archivo = ar

if ruta_archivo is None:
    ayuda()

with open(ruta_archivo, 'r') as archivo:
    invertir_lineas(archivo)
