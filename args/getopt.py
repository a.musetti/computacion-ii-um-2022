#!/usr/bin/env python3
# Requerimos Python 3

import getopt, sys, math

(opt,arg) = getopt.getopt(sys.argv[1:], 'o:n:m:')

def conv_num(s):
	try:
		return float(s)
	except:
		print('ERROR: debe ingresar un número', file=sys.stderr)
		sys.exit(1)

n=None
m=None
oper=None
for (op,ar) in opt:
	if op == '-o':
		if ar in ['+', '-', '*', '/']:
			oper=ar
		else:
			print('ERROR: el operador debe ser uno de los siguientes: +-*/', file=sys.stderr)
			sys.exit(1)
	elif op == '-n':
		n = conv_num(ar)
	elif op == '-m':
		m = conv_num(ar)

if oper is None:
	print('ERROR: falta ingresar operador con -o OPERADOR', file=sys.stderr)
	sys.exit(1)
if m is None:
	print('ERROR: falta ingresar primer operando con -m NÚMERO', file=sys.stderr)
	sys.exit(1)
if n is None:
	print('ERROR: falta ingresar segundo operando con -n NÚMERO', file=sys.stderr)
	sys.exit(1)

if n == 0.0 and oper == '/':
	print('ERROR: división por cero no está definida.', file=sys.stderr)
	sys.exit(1)

r=None
match oper:
	case '+': r = m + n
	case '-': r = m - n
	case '*': r = m * n
	case '/': r = m / n

print('%f %s %f = %f' % (m, oper, n, r))
