#!/usr/bin/env python3
# Requerimos Python 3

import getopt, sys, os
from typing import NoReturn

import socket as sk


def ayuda() -> NoReturn:
    print('''uso: %s -h DIRECCION -p PUERTO
    ''' % sys.argv[0], file=stderr)
    sys.exit(1)

# Main.

TIMEOUT=0.05 # Segundos
BUFFER_SIZE=1024 # Bytes
dirservidor = None
puerto = None

#=====================================================#

(opt,arg) = getopt.getopt(sys.argv[1:], 'p:h:')
for (op,ar) in opt:
    if op == '-p':
        puerto = int(ar)
    elif op == '-h':
        dirservidor = ar

if puerto is None or dirservidor is None:
        ayuda()

#-----------------------------------------------------#

def leer(sock):
    # Acumular respuesta, que puede ser muy grande.
    mensaje = b""
    while True:
        try:
            datos = sock.recv(BUFFER_SIZE)
            mensaje = mensaje + datos
            if len(datos) < BUFFER_SIZE:
                break
        except TimeoutError:
            break
    return mensaje.decode()

#-----------------------------------------------------#

srvsocket = sk.socket(sk.AF_INET,sk.SOCK_STREAM)
srvsocket.settimeout(TIMEOUT)# Por si no hay respuesta.
srvsocket.connect((dirservidor,puerto))

while True:
    cmd = sys.stdin.readline()
    if (len(cmd) == 0) or (cmd == "bye\n"):
        break
    srvsocket.sendall(cmd.encode())
    print(leer(srvsocket))
srvsocket.close()

#-----------------------------------------------------#
