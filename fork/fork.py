#!/usr/bin/env python3
# Requerimos Python 3

import getopt, sys, os
from typing import NoReturn

def ayuda() -> NoReturn:
    print('''uso: %s -n NRO_HIJOS [-h] [-v]
	-n NRO_HIJOS  ejecuta el programa con N subprocesos.
	-h Muestra esta ayuda y termina.
	-v Modo verboso, muestra inicio/fin de cada subproceso.
    ''' % sys.argv[0])
    sys.exit(0)

def suma_pares(verboso) -> NoReturn:
    pid = os.getpid()
    ppid = os.getppid()
    if verboso: print('Iniciando proceso hijo %d.' % pid)

    # Suma de 1 a pid.
    val=sum(range(1,pid+1))
    print('%d - %d: %d' % (pid,ppid,val))

    if verboso: print('Terminado proceso hijo %d.' % pid)

    # NoReturn
    sys.exit(0)

def lanzar_hijos(nro_hijos,modo_verboso):
    for h in range(nro_hijos):
        if os.fork() == 0:
            # En el hijo, sumar pares.
            suma_pares(modo_verboso)
            sys.exit(-1) # Redundante pero seguro.
    # De vuelta en el padre, esperar a los hijos.
    os.wait()

# Main.

(opt,arg) = getopt.getopt(sys.argv[1:], 'n:hv')

verboso=False
nhijos=0
for (op,ar) in opt:
    if op == '-n':
        nhijos=int(ar)
    elif op == '-h':
        ayuda()
    elif op == '-v':
        verboso=True

if nhijos == 0:
    ayuda()
else:
    lanzar_hijos(nhijos,verboso)
