#!/usr/bin/env python3
# Requerimos Python 3

import sys,os,codecs
from typing import NoReturn
from multiprocessing import Process,Pipe,Queue

def entrada_salida(tubo,cola):
	# sys.stdin en el hijo inicialmente apunta a
	# /dev/null, abrir /dev/stdin para poder leer.
	sys.stdin=open(0)
	while True:
		linea=sys.stdin.readline()
		tubo.send(linea)
		# En UNIX/Linux terminar con Ctrl+D
		if len(linea) == 0:
			return
		rot13=cola.get()
		print(rot13)

def cifrado(tubo,cola):
	while True:
		linea=tubo.recv()
		# En UNIX/Linux terminar con Ctrl+D
		if len(linea) == 0:
			return
		rot13=codecs.encode(linea, 'rot_13')
		cola.put(rot13)

# Main.
cola = Queue()
tubo_recibir, tubo_mandar = Pipe(duplex=False)
h1_entsal = Process(target=entrada_salida, args=(tubo_mandar,cola))
h2_cifrado = Process(target=cifrado, args=(tubo_recibir,cola))

h1_entsal.start()
h2_cifrado.start()

h1_entsal.join()
h2_cifrado.join()
