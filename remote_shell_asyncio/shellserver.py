#!/usr/bin/env python3
# Requerimos Python 3

import getopt, sys, os
from typing import NoReturn

import asyncio as aio

def ayuda() -> NoReturn:
    print('''uso: %s -p PUERTO''' % sys.argv[0], file=sys.stderr)
    sys.exit(1)

# Main.

BUFFER_SIZE=1024
puerto = None

#=====================================================#

def log(msg):
    print(msg,file=sys.stderr)

async def run_prog(cmd):
    # Consolidar stdout y stderr
    proc = await aio.create_subprocess_shell(
        cmd
        ,stdout=aio.subprocess.PIPE
        ,stderr=aio.subprocess.STDOUT
    )
    salida,stderr = await proc.communicate()
    await proc.wait()
    if proc.returncode == 0:
        salida = b'OK\n' + salida
    else:
        salida = b'ERROR\n' + salida
    return salida

async def handle_shell_client(reader,writer):
    while True:
        cmd = await reader.read(BUFFER_SIZE)
        if len(cmd) == 0: # Largo en bytes.
            break
        cmd = cmd.decode().strip()
        if len(cmd) > 0: # Largo en caracteres.
            addr = writer.get_extra_info('peername')
            log("recibido comando '%s' desde %s" % (cmd,addr))
            salida = await run_prog(cmd)
            log('enviados %d bytes a %s' % (len(salida),addr))
            writer.write(salida)
            await writer.drain()
    log('cerrando conexion con %s' % (addr,))
    writer.close()


async def start_shell_server(host,port):
    srv = await aio.start_server(handle_shell_client,host,port)
    dirs = ','.join(str(sock.getsockname()) for sock in srv.sockets)
    log(f'iniciando servidor atendiendo en {dirs}')
    async with srv:
        await srv.serve_forever()
#=====================================================#

(opt,arg) = getopt.getopt(sys.argv[1:], 'p:')
for (op,ar) in opt:
    if op == '-p':
        puerto = int(ar)

if puerto is None:
    ayuda()

#-----------------------------------------------------#

aio.run(start_shell_server('localhost',puerto))

#-----------------------------------------------------#
