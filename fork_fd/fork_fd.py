#!/usr/bin/env python3
# Requerimos Python 3

import getopt, sys, os, time

def ayuda():
    print('''uso: %s -n HIJOS -r VECES -f ARCHIVO [-h] [-v]
	-n HIJOS Invocar HIJOS cantidad de subprocesos.
	-r VECES Imprimir VECES cantidad de letras en cada subproceso.
	-v Modo verboso, muestra datos adicionales.
	-h Muestra esta ayuda y termina.
    ''' % sys.argv[0])
    sys.exit(0)


def escribir_letra(letra,veces,salida,verboso):
    for i in range(veces):
        time.sleep(1)
        if verboso:
            print("Proceso %d escribiendo letra '%c'" % (os.getpid(), letra)
                  , file=salida)
        else:
            salida.write(letra)
        salida.flush()

# Main.
nhijos=0
rveces=0
ruta_archivo=None
verboso=False

(opt,arg) = getopt.getopt(sys.argv[1:], 'n:r:f:hv')
for (op,ar) in opt:
    if op == '-n':
        nhijos=int(ar)
    elif op == '-r':
        rveces=int(ar)
    elif op == '-f':
        ruta_archivo=ar
    elif op == '-v':
        verboso=True
    elif op == '-h':
        ayuda()

if (nhijos <= 0) or (rveces <= 0) or (ruta_archivo is None):
    ayuda()

with open(ruta_archivo, 'w') as salida:
    for h in range(nhijos):
        letra=chr(ord('A') + h)
        if os.fork() == 0:
            escribir_letra(letra, rveces, salida, verboso)
            sys.exit(0) # NoReturn.
    # Esperar hijos.
    os.wait()
