#!/usr/bin/env python3
# Requerimos Python 3

import getopt, sys, os
from typing import NoReturn

import socket as sk
import subprocess as sp
import threading as th
import multiprocessing as mp


def ayuda() -> NoReturn:
    print('''uso: %s -p PUERTO -c CONCURRENCIA
CONCURRENCIA:
   p    Procesos
   t    Hilos
    ''' % sys.argv[0], file=sys.stderr)
    sys.exit(1)

# Main.

PROCESOS = 'p'
HILOS = 't'

BUFFER_SIZE=1024
puerto = None
tipo = None

#=====================================================#

def log(msg):
    print(msg,file=sys.stderr)

def run_prog(cmd):
    # Consolidar stdout y stderr
    resultado = sp.run(cmd,stdout=sp.PIPE,stderr=sp.STDOUT
                       ,shell=True,text=True)
    salida = resultado.stdout
    if resultado.returncode == 0:
        salida = "OK\n" + salida
    else:
        salida = "ERROR\n" + salida
    return salida.encode()

def handle_shell_client(clisock,addr):
    while True:
        cmd = clisock.recv(BUFFER_SIZE)
        if len(cmd) == 0: # Largo en bytes.
            break
        cmd = cmd.decode().strip()
        if len(cmd) > 0: # Largo en caracteres.
            log("recibido comando '%s' desde %s" % (cmd,addr))
            salida = run_prog(cmd)
            log('enviados %d bytes a %s' % (len(salida),addr))
            clisock.sendall(salida)
    log('cerrando conexion con %s' % (addr,))
    clisock.close()

def start_shell_server(host,port,qsize,mode):
    dirs = sk.getaddrinfo(host,port,sk.AF_UNSPEC,sk.SOCK_STREAM)
    for dir in dirs:
        args=(dir,host,port,qsize,mode)
        th.Thread(target=spawn_shell_server_with_address,args=args,daemon=True).start()
    for t in th.enumerate():
        if t != th.main_thread():
            t.join()

def spawn_shell_server_with_address(addr,host,port,qsize,mode):
    srvsock = sk.socket(addr[0],addr[1])
    srvsock.setsockopt(sk.SOL_SOCKET, sk.SO_REUSEADDR, 1)
    srvsock.bind((host,port))
    srvsock.listen(qsize)
    while True:
        clisock,addr = srvsock.accept()
        cliproc = None
        args=(clisock,addr,)
        if mode == PROCESOS:
            mp.Process(target=handle_shell_client,args=args,daemon=True).start()
        elif mode == HILOS:
            th.Thread(target=handle_shell_client,args=args,daemon=True).start()
        else:
            raise

#=====================================================#

(opt,arg) = getopt.getopt(sys.argv[1:], 'p:c:')
for (op,ar) in opt:
    if op == '-p':
        puerto = int(ar)
    elif op == '-c':
        tipo = ar

if puerto is None or tipo is None:
    ayuda()

if tipo not in [PROCESOS,HILOS]:
    ayuda()

#-----------------------------------------------------#

start_shell_server(host="localhost",port=puerto,qsize=3,mode=tipo)

#-----------------------------------------------------#
