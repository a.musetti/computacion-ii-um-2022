#!/usr/bin/env python3
# Requerimos Python 3

import sys,codecs
from threading import Thread
from queue import Queue

def entrada_salida(cola_txt, cola_cif):
	while True:
		linea=sys.stdin.readline()
		cola_txt.put(linea)
		# En UNIX/Linux terminar con Ctrl+D
		if len(linea) == 0:
			return
		rot13=cola_cif.get()
		cola_cif.task_done()
		print(rot13)

def cifrado(cola_txt,cola_cif):
	while True:
		linea=cola_txt.get()
		cola_txt.task_done()
		# En UNIX/Linux terminar con Ctrl+D
		if len(linea) == 0:
			return
		rot13=codecs.encode(linea, 'rot_13')
		cola_cif.put(rot13)

# Main.

# Las colas son seguras para usar con hilos.
cola_txt = Queue()
cola_cif = Queue()

h1_entsal = Thread(target=entrada_salida, args=(cola_txt,cola_cif),daemon=True)
h2_cifrado = Thread(target=cifrado, args=(cola_txt,cola_cif),daemon=True)

h1_entsal.start()
h2_cifrado.start()

cola_txt.join()
cola_cif.join()

h1_entsal.join()
h2_cifrado.join()
