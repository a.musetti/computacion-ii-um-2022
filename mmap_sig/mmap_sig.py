#!/usr/bin/env python3
# Requerimos Python 3

import getopt, sys, os, mmap, signal, time
from typing import NoReturn

BUFFER_SIZE=200
hijos = []
memoria = None
archivo = None

def ayuda() -> NoReturn:
    print('''uso: %s -f ARCHIVO
	Escribe la entrada del usuario en ARCHIVO,
	con todas las letras en mayúsculas.
    ''' % sys.argv[0], file=sys.stderr)
    sys.exit(1)

def hijo(proc, **kwargs):
    global hijos
    h = os.fork()
    if h == 0:
        proc(**kwargs)
        sys.exit(0) # NoReturn
    hijos.append(h)

def sig_hijo2_usr1(sig,frame):
    global memoria
    global archivo
    memoria.seek(0)
    archivo.write(memoria.readline().decode().upper())
    archivo.flush()
    # Vaciar memoria con ceros.
    memoria.seek(0)
    memoria.write(bytearray(len(memoria)))


def sig_hijo2_usr2(sig,frame):
    sys.exit(0)


def hijo1_leer_entrada(**kwargs):
    global memoria
    while True:
        data=sys.stdin.readline().encode()
        if len(data) == 0: continue
        if data == b"bye\n":
           os.kill(os.getppid(), signal.SIGUSR2)
           sys.exit(0)
        memoria.seek(0)
        memoria.write(data)
        # Notificar lectura.
        os.kill(os.getppid(), signal.SIGUSR1)

def hijo2_guardar_salida(**kwargs):
    global memoria
    global archivo
    ### Instalar handler USR1,USR2 h2.
    signal.signal(signal.SIGUSR1, sig_hijo2_usr1)
    signal.signal(signal.SIGUSR2, sig_hijo2_usr2)
    print(kwargs)
    # Esperar alguna señal.
    while True:
        signal.pause()

def sig_padre_usr1(sig,frame):
    global hijos
    global memoria
    memoria.seek(0)
    print(memoria.readline()[:-1].decode())
    os.kill(hijos[1], signal.SIGUSR1)
    
def sig_padre_usr2(sig,frame):
    global hijos
    os.kill(hijos[1], signal.SIGUSR2)

def guardar_entrada_en_mayusculas():

    ### Instalar handler USR1,USR2 padre.
    signal.signal(signal.SIGUSR1, sig_padre_usr1)
    signal.signal(signal.SIGUSR2, sig_padre_usr2)

    hijo(hijo1_leer_entrada)
    hijo(hijo2_guardar_salida)

    # Espero hijos y termino.
    # Desde ahora se maneja todo por entrada y señales.
    os.wait()

# Main.

ruta_archivo = None

(opt,arg) = getopt.getopt(sys.argv[1:], 'f:')
for (op,ar) in opt:
    if op == '-f':
        ruta_archivo = ar

if ruta_archivo is None:
    ayuda()

# Abrir archivo de salida.
with open(ruta_archivo,'w') as a:
    archivo = a
    # Reservar área de memoria anónima de 2KiB.
    with mmap.mmap(-1, BUFFER_SIZE) as m:
        memoria = m
        guardar_entrada_en_mayusculas()
