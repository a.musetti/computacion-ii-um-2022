#!/usr/bin/env python3
# Requerimos Python 3

import getopt, sys, os, math
from typing import NoReturn
from multiprocessing import Pool

def ayuda() -> NoReturn:
    print('''uso: %s -p PROCESOS -c FUNCION -f ARCHIVO
FUNCION: raiz, pot, log
    ''' % sys.argv[0], file=sys.stderr)
    sys.exit(1)



# Main.

# Datito para recordar:
#   las funciones para Pool.map(func,data) deben ser "Pickleables",
#   no puedo pasarle lambdas directamente sin hacer malabares
#   que no valen la pena para esto.
#   Por eso uso funciones normales.

def raiz (fila) : return [math.sqrt(x)   for x in fila]
def pot  (fila) : return [math.pow(x,x)  for x in fila]
def log  (fila) : return [math.log(x,10) for x in fila]
mapa_funciones={
'raiz' : raiz,
'pot'  : pot,
'log'  : log,
}

ruta_archivo = None
num_procesos = None
funcion = None

(opt,arg) = getopt.getopt(sys.argv[1:], 'p:c:f:')
for (op,ar) in opt:
    if op == '-f':
        ruta_archivo = ar
    elif op == '-p':
        num_procesos = int(ar)
    elif op == '-c':
        funcion = ar


for param in [ruta_archivo, num_procesos, funcion]:
	if param is None:
		ayuda()

if funcion not in mapa_funciones.keys():
	ayuda()

def matriz(archivo):
	# OJO: SIN CONTROL DE ERRORES.
	# Asumimos entrada bien formada:
	#  - todas las filas del mismo largo.
	#  - cada fila con todos los separadores.
	#  - números decimales en cada celda.
	return [[float(x.strip()) for x in linea.strip().split(',')]
		 for linea in archivo]

def vermat(m):
	for f in m:
		for x in f:
			print("%15.3f" % x,end='')
		print()

with open(ruta_archivo,'r') as archivo:
	m=matriz(archivo)
	f=mapa_funciones[funcion]
	with Pool(num_procesos) as p:
		r=p.map(f, m)
		vermat(r)
