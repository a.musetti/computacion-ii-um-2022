#!/usr/bin/env python3
# Requerimos Python 3

import getopt, sys
import shlex, subprocess as sp
from datetime import datetime

def ejecutar_comando(comando,salida,registro):
    cmdline = shlex.split(comando)
    with sp.Popen(cmdline,stdout=sp.PIPE,stderr=sp.PIPE,text=True) as p:

        # Ejecuto proceso, obtengo datos de salida/error y espero cod. de retorno.
        output_data,error_data = p.communicate()

        # Guardo stdout.
        salida.write(output_data)

        # Guardo mensaje de log.
        # ARREGLAR: se pierden los datos de stderr del proceso
        # cuando este termina exitosamente.
        # No me queda claro en la consigna si hay que guardar
        # stderr en el log o en la salida en este caso.
        # Por lo que interpreto, se debe ignorar, que es lo que hago.
        fechayhora=datetime.now()        
        if p.returncode == 0:
            print('%s: Comando "%s" ejecutado correctamente.\n' % (fechayhora,comando),
                  file=registro)
        else:
            print('%s: %s\n' % (fechayhora,error_data),file=registro)

# Main.

(opt,arg) = getopt.getopt(sys.argv[1:], 'c:f:l:')

# Por defecto escribo en stdout y stderr.
output_file=sys.stdout
log_file=sys.stderr

# Leer argumentos.
command=None
output_filename=None
log_filename=None

for (op,ar) in opt:
    if op == '-c':
        command=ar
    elif op == '-f':
        output_filename=ar
    elif op == '-l':
        log_filename=ar

if command is None:
    print('uso: %s -c COMANDO [-f ARCHIVO_SALIDA] [-l ARCHIVO_LOG]' % sys.argv[0],
          file=sys.stderr)
    sys.exit(1)

# Abrir archivos de salida.
if output_filename is not None:
    output_file=open(output_filename, mode='a')
if log_filename is not None:
    log_file=open(log_filename, mode='a')

ejecutar_comando(command, output_file, log_file)

# Cerrar archivos de salida.
# No pasa nada si cerramos stdout/stderr.
log_file.close()
output_file.close()
